/**
 * @file
 * Initializes the Virtual Select element.
 */
(function ($, Drupal, once) {
  'use strict';
  Drupal.behaviors.virtualSelect = {
    attach(context, settings) {
      $(once('virtualSelect', '.virtual-select', context)).each(function (i, element) {
        const { id } = element;
        const elementId = `#${id}`;

        $(elementId).change(function () {
          // Set input on hidden select field.
          $(`.form-select[data-virtual-select="${id}"]`).val(this.value);
        });

        const options = $.extend(true, settings[id], {ele: elementId});
        VirtualSelect.init(options);
      });
    },
  };
})(jQuery, Drupal, once);
