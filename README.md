# Virtual Select

Provides an integration with the [Virtual Select](https://sa-si-dev.github.io/virtual-select) library for select form elements.

## Features
* Support more than 100.000 dropdown options.
* Support multi-select.
* Support search feature.

## Dependencies
This module requires you to install the JavaScript library [Virtual Select](https://sa-si-dev.github.io/virtual-select).

## Installation
You can install the module like any other Drupal module. For more information, please consult [https://www.drupal.org/docs/extending-drupal/installing-modules](https://www.drupal.org/docs/extending-drupal/installing-modules).

### Composer
It is recommended to install the Virtual Select JavaScript library with
Composer.

In the **require** section of your composer.json, add the virtual-select-plugin
JavaScript library.

```json
"require": {
    "drupal/virtual_select": "^1.0.0",
    "npm-asset/virtual-select-plugin": "^1.0.38"
}
```

In the **repositories** section of your composer.json, add the asset-packagist
repository.

```json
"repositories": [
    {
        "type": "composer",
        "url": "https://asset-packagist.org"
    }
],
```

In the **installer-types** section of your composer.json, add the npm-asset
type.

```json
"installer-types": [
    "bower-asset",
    "npm-asset"
],
```

In the **installer-paths** section of your composer.json, add the virtual
select library.

```json
"web/libraries/{$name}": [
    "type:drupal-library",
    "npm-asset/virtual-select-plugin"
],
```

## Usage
This module provides an implementation for **@FormElement** and **@FieldWidget**. You can use the element like any other form element. For more information, please consult [https://api.drupal.org/api/drupal/elements](https://api.drupal.org/api/drupal/elements).

### Example
```php
$form['virtual_select'] = [
  '#type' => 'virtual_select',
  '#title' => $this->t('My Virtual Select element'),
  '#options' => [
    'cheese' => $this->t('Cheese'),
    'eggs' => $this->t('Eggs'),
    'milk' => $this->t('Milk'),
    'rice' => $this->t('Rice'),
    'chicken' => $this->t('Chicken'),
    'bread' => $this->t('Bread'),
  ],
  '#default_value' => ['chicken', 'milk'],
  '#multiple' => TRUE,
  '#placeholder' => $this->t('- Select -'),
  '#search' => TRUE,
  '#show_value_as_tags' => TRUE,
  '#show_selected_options_first' => TRUE,
  '#show_options_only_on_search' => FALSE,
  '#search_placeholder' => $this->t('Search...'),
  '#no_options_text' => $this->t('No options found.'),
  '#select_all_text' => $this->t('Select all'),
  '#all_options_selected_text' => $this->t('All'),
  '#disabled_options' => ['rice', 'bread'],
];
```
