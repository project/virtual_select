<?php

declare(strict_types = 1);

namespace Drupal\virtual_select\Element;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;

/**
 * Defines a form element implementation for the Virtual Select library.
 *
 * @FormElement("virtual_select")
 */
class VirtualSelect extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo(): array {
    $class = static::class;
    $info = [
      '#input' => TRUE,
      '#multiple' => FALSE,
      '#process' => [
        [$class, 'processElement'],
      ],
      '#theme' => 'virtual_select',
      '#theme_wrappers' => ['form_element'],
    ];

    foreach (self::getDefinition() as $key => $definition) {
      $info['#' . $key] = $definition['default_value'];
    }

    return $info;
  }

  /**
   * Retrieve the Virtual Select element definition.
   *
   * @return array[]
   *   The definition.
   */
  public static function getDefinition(): array {
    return [
      'search' => [
        'property' => 'search',
        'type' => 'checkbox',
        'label' => t('Search'),
        'description' => t('Enable search feature.'),
        'default_value' => TRUE,
      ],
      'show_value_as_tags' => [
        'property' => 'showValueAsTags',
        'type' => 'checkbox',
        'label' => t('Show value as tags'),
        'description' => t('Show each selected values as tags with remove icon.'),
        'default_value' => TRUE,
      ],
      'show_selected_options_first' => [
        'property' => 'showSelectedOptionsFirst',
        'type' => 'checkbox',
        'label' => t('Show selected options first'),
        'description' => t('Show selected options at the top of the dropbox.'),
        'default_value' => TRUE,
      ],
      'show_options_only_on_search' => [
        'property' => 'showOptionsOnlyOnSearch',
        'type' => 'checkbox',
        'label' => t('Require search to show options'),
        'description' => t('Show options to select only if search value is not empty.'),
        'default_value' => FALSE,
      ],
      'placeholder' => [
        'property' => 'placeholder',
        'type' => 'textfield',
        'label' => t('Placeholder'),
        'description' => t('Text to show when no options selected.'),
        'default_value' => t('- Select -'),
      ],
      'search_placeholder' => [
        'property' => 'searchPlaceholderText',
        'type' => 'textfield',
        'label' => t('Search placeholder'),
        'description' => t('Text to show as placeholder for search input.'),
        'default_value' => t('Search...'),
      ],
      'no_options_text' => [
        'property' => 'noOptionsText',
        'type' => 'textfield',
        'label' => t('No options text'),
        'description' => t('Text to show when no options to show.'),
        'default_value' => t('No options found.'),
      ],
      'select_all_text' => [
        'property' => 'selectAllText',
        'type' => 'textfield',
        'label' => t('Select all text'),
        'description' => t('Text to show near select all checkbox when search is disabled.'),
        'default_value' => t('Select All'),
      ],
      'all_options_selected_text' => [
        'property' => 'allOptionsSelectedText',
        'type' => 'textfield',
        'label' => t('All options selected text'),
        'description' => t('Text to use when displaying all values selected text (i.e. All (10)).'),
        'default_value' => t('All'),
      ],
      'disabled_options' => [
        'property' => 'disabledOptions',
        'type' => 'virtual_select',
        'label' => t('Disabled options'),
        'description' => t('List of values to disable options.'),
        'default_value' => [],
      ],
    ];
  }

  /**
   * Processes the virtual_select element.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $complete_form
   *   The complete form.
   *
   * @return array
   *   The processed element.
   */
  public static function processElement(array &$element, FormStateInterface $form_state, array &$complete_form): array {
    $id = Html::getUniqueId('virtual-select');

    $multiple = $element['#multiple'] ?? FALSE;
    $element['#tree'] = TRUE;
    $element['#attributes']['id'] = $id;
    $element['#attributes']['class'][] = 'virtual-select';
    $element['#attached']['library'][] = 'virtual_select/virtual_select';

    foreach ($element['#options'] ?? [] as $value => $label) {
      $element['#attached']['drupalSettings'][$id]['options'][] = [
        'label' => $label,
        'value' => $value,
      ];
    }

    $element['#attached']['drupalSettings'][$id]['selectedValue'] = $element['#default_value'] ?? [];
    $element['#attached']['drupalSettings'][$id]['disabledOptions'] = $element['#disabled_options'] ?? [];
    $element['#attached']['drupalSettings'][$id]['multiple'] = $multiple;

    foreach (self::getDefinition() as $key => $definition) {
      if ($definition['type'] === 'checkbox') {
        $element['#attached']['drupalSettings'][$id][$definition['property']] = (bool) $element['#' . $key];
        continue;
      }

      if ($definition['type'] === 'virtual_select') {
        $element['#attached']['drupalSettings'][$id][$definition['property']] = (array) $element['#' . $key] ?? [];
        continue;
      }

      $element['#attached']['drupalSettings'][$id][$definition['property']] = $element['#' . $key] ?? NULL;
    }

    $element['input'] = [
      '#type' => 'select',
      '#options' => $element['#options'] ?? [],
      '#default_value' => $element['#default_value'] ?? [],
      '#element_validate' => [],
      '#multiple' => $multiple,
      '#parents' => [$element['#name'] . '-input'],
      '#attributes' => [
        'data-virtual-select' => $id,
        'class' => ['hidden'],
      ],
      '#wrapper_attributes' => [
        'class' => ['hidden'],
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state): mixed {
    $fieldName = $element['#name'] . '-input';
    $form_state->addCleanValueKey($element['#name'] . '-input');
    return $form_state->getUserInput()[$fieldName] ?? [];
  }

}
