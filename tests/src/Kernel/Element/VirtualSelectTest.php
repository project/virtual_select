<?php

namespace Drupal\Tests\virtual_select\Kernel\Element;

use Drupal\Core\Form\FormState;
use Drupal\KernelTests\KernelTestBase;
use Drupal\virtual_select\Element\VirtualSelect;

/**
 * @coversDefaultClass \Drupal\virtual_select\Element\VirtualSelect
 *
 * @group virtual_select
 */
class VirtualSelectTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['system', 'virtual_select'];

  /**
   * Tests the VirtualSelect form element.
   */
  public function testVirtualSelectElement() {
    $options = [
      'cheese' => 'Cheese',
      'eggs' => 'Eggs',
      'milk' => 'Milk',
      'rice' => 'Rice',
      'chicken' => 'Chicken',
      'bread' => 'Bread',
    ];

    $element = [
      '#type' => 'virtual_select',
      '#name' => 'virtual-select',
      '#title' => 'My Virtual Select element',
      '#options' => $options,
      '#default_value' => ['chicken', 'milk'],
      '#multiple' => TRUE,
      '#placeholder' => '- Select -',
      '#search' => TRUE,
      '#show_value_as_tags' => TRUE,
      '#show_selected_options_first' => TRUE,
      '#show_options_only_on_search' => FALSE,
      '#search_placeholder' => 'Search...',
      '#no_options_text' => 'No options found.',
      '#select_all_text' => 'Select all',
      '#all_options_selected_text' => 'All',
      '#disabled_options' => ['rice', 'bread'],
    ];

    $completeForm = [];
    $processedElement = VirtualSelect::processElement($element, new FormState(), $completeForm);

    // Assert that the element is processed correctly.
    $this->assertArrayHasKey('input', $processedElement);
    $this->assertEquals('virtual_select', $processedElement['#type']);
    $this->assertEquals('select', $processedElement['input']['#type']);
    $this->assertEquals($options, $processedElement['input']['#options']);
    $this->assertEquals(['chicken', 'milk'], $processedElement['input']['#default_value']);
    $this->assertEquals('virtual-select', $processedElement['#attributes']['id']);
    $this->assertEquals(['virtual-select'], $processedElement['#attributes']['class']);
    $this->assertEquals(array_keys($options), array_column($processedElement['#attached']['drupalSettings']['virtual-select']['options'], 'value'));
    $this->assertEquals(array_values($options), array_column($processedElement['#attached']['drupalSettings']['virtual-select']['options'], 'label'));
    $this->assertEquals(['chicken', 'milk'], $processedElement['#attached']['drupalSettings']['virtual-select']['selectedValue']);
    $this->assertEquals(['rice', 'bread'], $processedElement['#attached']['drupalSettings']['virtual-select']['disabledOptions']);
    $this->assertTrue($processedElement['#attached']['drupalSettings']['virtual-select']['multiple']);
    $this->assertTrue($processedElement['#attached']['drupalSettings']['virtual-select']['search']);
    $this->assertTrue($processedElement['#attached']['drupalSettings']['virtual-select']['showValueAsTags']);
    $this->assertTrue($processedElement['#attached']['drupalSettings']['virtual-select']['showSelectedOptionsFirst']);
    $this->assertFalse($processedElement['#attached']['drupalSettings']['virtual-select']['showOptionsOnlyOnSearch']);
    $this->assertEquals('Search...', $processedElement['#attached']['drupalSettings']['virtual-select']['searchPlaceholder']);
    $this->assertEquals('No options found.', $processedElement['#attached']['drupalSettings']['virtual-select']['noOptionsText']);
    $this->assertEquals('Select all', $processedElement['#attached']['drupalSettings']['virtual-select']['selectAllText']);
    $this->assertEquals('All', $processedElement['#attached']['drupalSettings']['virtual-select']['allOptionsSelectedText']);
  }

}
