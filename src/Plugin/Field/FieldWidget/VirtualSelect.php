<?php

declare(strict_types = 1);

namespace Drupal\virtual_select\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\virtual_select\Element\VirtualSelect as VirtualSelectElement;

/**
 * Provides the Virtual Select widget.
 *
 * @FieldWidget(
 *   id = "virtual_select",
 *   label = @Translation("Virtual Select"),
 *   field_types = {
 *     "entity_reference",
 *     "list_integer",
 *     "list_float",
 *     "list_string"
 *   },
 *   multiple_values = TRUE
 * )
 */
class VirtualSelect extends OptionsWidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    $settings = parent::defaultSettings();
    foreach (VirtualSelectElement::getDefinition() as $key => $definition) {
      $settings[$key] = $definition['default_value'];
    }
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $element = [];
    foreach (VirtualSelectElement::getDefinition() as $key => $definition) {
      // @todo Add support for disabled options.
      if ($key === 'disabled_options') {
        continue;
      }

      $element[$key] = [
        '#type' => $definition['type'],
        '#title' => $definition['label'],
        '#default_value' => $this->getSetting($key),
        '#description' => $definition['description'],
      ];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary['search'] = $this->t('Search: @value', [
      '@value' => $this->getSetting('search') ? $this->t('Enabled') : $this->t('Disabled'),
    ]);
    $summary['show_value_as_tags'] = $this->t('Show value as tags: @value', [
      '@value' => $this->getSetting('show_value_as_tags') ? $this->t('Enabled') : $this->t('Disabled'),
    ]);
    $summary['show_selected_options_first'] = $this->t('Show selected options first: @value', [
      '@value' => $this->getSetting('show_selected_options_first') ? $this->t('Enabled') : $this->t('Disabled'),
    ]);
    $summary['show_options_only_on_search'] = $this->t('Require search to show options: @value', [
      '@value' => $this->getSetting('show_options_only_on_search') ? $this->t('Enabled') : $this->t('Disabled'),
    ]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element += [
      '#type' => 'virtual_select',
      '#options' => $this->getOptions($items->getEntity()),
      '#default_value' => $this->getSelectedOptions($items),
      '#multiple' => $this->multiple && count($this->options) > 1,
    ];

    foreach ($this->getSettings() as $key => $value) {
      $element['#' . $key] = $value;
    }

    return $element;
  }

}
